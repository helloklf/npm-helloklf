/**
 * 一套拓展的历史状态管理机制，便于实现h5界面中无刷新的页面导航
 */
export default (function () {
    let popstateCallbacks = [];
    let pagebackCallback = {};

    let viewstate = {
        /** 增加状态
         * @param {Any} obj 要存储的对象
         * @param {String} title 页面标题
         * @param {String} url 页面链接
         */
        pushState(obj, title, url) {
            if (window.history.state == null) {
                viewstate.setState();
            }
            window.history.pushState(
                {
                    "obj": obj || {},
                    "title": title || "",
                    "symbol": "sads"
                },
                title || "",
                url || ""
            );
        },
        /** 替换状态
         * @param {Any} obj 要存储的对象
         * @param {String} title 页面标题
         * @param {String} url 页面链接
         */
        replaceState(obj, title, url) {
            window.history.replaceState(
                {
                    "obj": obj || {},
                    "title": title || "",
                    "symbol": " Symbol()"
                },
                title || "",
                url || ""
            );
        },
        /** 设置状态
         * @param {Any} obj 要存储的对象
         * @param {String} title 页面标题
         */
        setState(obj, title) {
            viewstate.replaceState({}, "", window.location.href);
        },
        /**
         * 获取当前history的state内容
         */
        getState() {
            if (window.history.state && window.history.state.symbol)
                return window.history.state.obj;
            else
                return window.history.state;
        },
        /** 监听页面后退事件
         * @param {Function} callback 页面后退时的回调方法，执行多次
         */
        onPopState(callback) {
            if (!popstateCallbacks.includes(callback)) {
                popstateCallbacks.push(callback);
            }
        },
        /**监听页面后退事件 once模式
         * @param {Function} callback 页面后退时的回调方法 只执行一次，且只相对于当前状态
         */
        onGoBack(callback) {
            if (window.history.state == null) {
                viewstate.setState();
            }

            let symbol = window.history.state.symbol;
            if (pagebackCallback[symbol] == null)
                pagebackCallback[symbol] = [];

            pagebackCallback[symbol].push(callback);
        }
    };

    window.addEventListener("popstate", function (e) {
        let symbol = (e.state) ? (e.state.symbol) : Symbol();
        let onceCallback = pagebackCallback[symbol];
        if (onceCallback != null) {
            onceCallback.forEach(callback => {
                callback((e.state.obj));
            });
        }
        pagebackCallback[symbol] = null;
        popstateCallbacks.forEach(callback => {
            callback((e.state.obj));
        });
    });
    return viewstate;
})();